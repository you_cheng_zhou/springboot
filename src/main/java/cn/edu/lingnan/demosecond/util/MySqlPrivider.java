package cn.edu.lingnan.demosecond.util;

import cn.edu.lingnan.demosecond.annotation.Id;
import cn.edu.lingnan.demosecond.annotation.Table;
import org.apache.ibatis.jdbc.SQL;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MySqlPrivider {

    public static final String INSERT = "insert";

    public static final String DELETE = "delete";

    public static final String UPDATE = "update";


    /**
     * 插入
     * @param obj
     * @return
     * @throws IllegalAccessException
     */
    public static String insert(Object obj) throws IllegalAccessException {
        Map<String,String> map = new HashMap<>();
        String table = getTableName(obj);//获取表名
        System.out.println(table);
        getMap(obj,map);

        return new SQL(){
            {
                //insert into table() values()
                INSERT_INTO(table);//insert into table
                for (String key :
                        map.keySet()) {
                    VALUES(key,map.get(key));
                }

            }

        }.toString();
    }

    /**
     * 删除
     * @param table
     * @param where
     * @return
     */
    public static String delete(String table,String where){
        if (StringUtil.isEmpty(table)){
            return null;
        }
        if (StringUtil.isEmpty(where)){
            return null;
        }

        return new SQL(){
            {
                DELETE_FROM(table);
                WHERE(where);
            }
        }.toString();

    }

    public static String update(Object obj) throws IllegalAccessException {
        Map<String,String> map = new HashMap<>();
        String table = getTableName(obj);
        String idName = getMap(obj,map);
        if (StringUtil.isEmpty(idName)){
            throw new RuntimeException("实体类-》"+obj.getClass().getCanonicalName()+"必须有@Id注解");
        }

        String idValue=map.get(idName);
        map.remove(idName);
        return new SQL(){
            {
                UPDATE(table);
                for (String key
                        :map.keySet()
                     ) {
                    SET(key+"="+map.get(key));

                }
                WHERE(idName+"="+idValue);
            }
        }.toString();
    }


    /**
     * 获取表名称
     * @param obj
     * @return
     */
    private static String  getTableName(Object obj){
        Class c =obj.getClass();
        Table table= (Table) c.getAnnotation(Table.class);
        if (table!=null){
            return table.value();
        }
        return StringUtil.underscoreName(c.getSimpleName());
    }

    /**
     * 获取被id注解的属性
     * @param obj
     * @return
     */
    private static String  getIdName(Field obj){
        Class c =obj.getClass();
        Id id= (Id) c.getAnnotation(Id.class);
        if (id!=null){
            return id.value();
        }
        return StringUtil.underscoreName(obj.getName());
    }

    /**
     * 将对象的非空属性放到map中，并且返回对象带有@Id注解的属性名或值
     * @param obj
     * @param map
     * @return
     * @throws IllegalAccessException
     */

    private static String getMap(Object obj,Map<String,String> map) throws IllegalAccessException {

        Class c = obj.getClass();
        Field[] fs = c.getDeclaredFields();//获取所有属性
        String idName=null;
        for (Field item :
                fs) {
            String key = item.getName();//获取列的属性名称
            item.setAccessible(true);//开始属性的权限才能拿到值
            if (idName == null){
                idName=getIdName(item);
            }
            if (item.get(obj) !=null){
                map.put(StringUtil.underscoreName(key),"#{"+key+"}");
            }

        }
        return idName;
    }


}
