package cn.edu.lingnan.demosecond.entity;

import cn.edu.lingnan.demosecond.annotation.Id;
import cn.edu.lingnan.demosecond.annotation.Table;

import java.io.Serializable;

/**
 * (RoleAuth)实体类
 *
 * @author makejava
 * @since 2020-06-05 16:41:47
 */
@Table("stu.role_auth")
public class RoleAuth implements Serializable {

   @Id
    private Integer id;
    
    private Integer roleId;
    
    private Integer meuId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getMeuId() {
        return meuId;
    }

    public void setMeuId(Integer meuId) {
        this.meuId = meuId;
    }

}