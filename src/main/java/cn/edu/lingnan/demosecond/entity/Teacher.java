package cn.edu.lingnan.demosecond.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Arrays;
@Component
@ConfigurationProperties(prefix = "nue.teacher")
public class Teacher {
    private String name;
    private int age;
    private String[] ablity;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String[] getAblity() {
        return ablity;
    }

    public void setAblity(String[] ablity) {
        this.ablity = ablity;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", ablity=" + Arrays.toString(ablity) +
                '}';
    }
}
