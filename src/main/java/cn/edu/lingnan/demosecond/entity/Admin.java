package cn.edu.lingnan.demosecond.entity;

import cn.edu.lingnan.demosecond.annotation.Id;
import cn.edu.lingnan.demosecond.annotation.Table;

import java.util.Date;
import java.io.Serializable;

/**
 * (Admin)实体类
 *
 * @author makejava
 * @since 2020-05-25 17:15:00
 */
@Table("stu.admin")
public class Admin implements Serializable {
    private static final long serialVersionUID = 429837937771628303L;

    @Id
    private Integer id;
    
    private String account;
    
    private String password;
    
    private String mobile;
    
    private Date regData;
    
    private Date lastLoginDate;
    
    private Integer isLlow;
    private String roles;

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getRegData() {
        return regData;
    }

    public void setRegData(Date regData) {
        this.regData = regData;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public Integer getIsLlow() {
        return isLlow;
    }

    public void setIsLlow(Integer isLlow) {
        this.isLlow = isLlow;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "id=" + id +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", mobile='" + mobile + '\'' +
                ", regData=" + regData +
                ", lastLoginDate=" + lastLoginDate +
                ", isLlow=" + isLlow +
                ", roles='" + roles + '\'' +
                '}';
    }
}