package cn.edu.lingnan.demosecond.service.impl;

import cn.edu.lingnan.demosecond.entity.Role;
import cn.edu.lingnan.demosecond.entity.RoleAuth;
import cn.edu.lingnan.demosecond.dao.RoleAuthDao;
import cn.edu.lingnan.demosecond.service.RoleAuthService;
import cn.edu.lingnan.demosecond.util.StringUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (RoleAuth)表服务实现类
 *
 * @author makejava
 * @since 2020-06-05 16:41:47
 */
@Service("roleAuthService")
public class RoleAuthServiceImpl implements RoleAuthService {
    @Resource
    private RoleAuthDao roleAuthDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public RoleAuth queryById(Integer id) {
        return this.roleAuthDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public IPage<RoleAuth> queryAllByLimit(int offset, int limit, RoleAuth bean) {
        Page<RoleAuth> page=new Page<>(offset,limit);
        page.setRecords(roleAuthDao.queryAll(page,bean));
        return page;
    }

    /**
     * 新增数据
     *
     * @param roleAuth 实例对象
     * @return 实例对象
     */
    @Override
    public int insert(RoleAuth roleAuth) {
        return this.roleAuthDao.insert(roleAuth);
    }

    /**
     * 修改数据
     *
     * @param roleAuth 实例对象
     * @return 实例对象
     */
    @Override
    public int update(RoleAuth roleAuth) {
        return  this.roleAuthDao.update(roleAuth);
    }

    /**
     * 通过主键删除数据
     *
     * @param ids 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(List<Integer> ids) {
        return this.roleAuthDao.delete("stu.role_auth", StringUtil.listToString(ids)) > 0;
    }
}