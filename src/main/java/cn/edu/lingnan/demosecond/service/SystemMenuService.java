package cn.edu.lingnan.demosecond.service;

import cn.edu.lingnan.demosecond.entity.SystemMenu;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 系统菜单表(SystemMenu)表服务接口
 *
 * @author makejava
 * @since 2020-05-27 16:51:42
 */
public interface SystemMenuService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    SystemMenu queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
   /* IPage<SystemMenu> queryAllByLimit(int offset, int limit,SystemMenu systemMenu);*/
    IPage<SystemMenu> queryAllByLimit(int offset, int limit, SystemMenu bean);

    /**
     * 新增数据
     *
     * @param systemMenu 实例对象
     * @return 实例对象
     */
    SystemMenu insert(SystemMenu systemMenu);

    /**
     * 修改数据
     *
     * @param systemMenu 实例对象
     * @return 实例对象
     */
    Boolean update(SystemMenu systemMenu);

    /**
     * 通过主键删除数据
     *
     * @param ids 主键
     * @return 是否成功
     */
    boolean deleteById(@Param("ids") List<Integer> ids);

    List<SystemMenu> queryAll();

}