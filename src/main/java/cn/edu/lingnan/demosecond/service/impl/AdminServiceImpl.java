package cn.edu.lingnan.demosecond.service.impl;

import cn.edu.lingnan.demosecond.entity.Admin;
import cn.edu.lingnan.demosecond.dao.AdminDao;
import cn.edu.lingnan.demosecond.entity.AdminQuery;
import cn.edu.lingnan.demosecond.service.AdminService;
import cn.edu.lingnan.demosecond.util.StringUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Admin)表服务实现类
 *
 * @author makejava
 * @since 2020-05-25 17:15:06
 */
@Service("adminService")
public class AdminServiceImpl implements AdminService {
    @Resource
    private AdminDao adminDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Admin queryById(Integer id) {
        return this.adminDao.queryById(id);
    }


    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public IPage<Admin> queryAllByLimit(int offset, int limit, AdminQuery bean) {
        Page<Admin> page=new Page<>(offset,limit);
        QueryWrapper<Admin> wrapper=new QueryWrapper<>();
        wrapper.like("account",bean.getAccount())
                .eq("mobile",bean.getMobile())
                .gt("reg_data",bean.getEndTime())
                .lt("reg_data",bean.getEndTime());
        //com.baomidou.mybatisplus.extension.plugins.pagination.Page<Admin> page = new Page<>(offset, limit);
        page.setRecords(adminDao.queryAll(page,bean));
        return page;
    }


    /**
     * 新增数据
     *
     * @param admin 实例对象
     * @return 实例对象
     */
    @Override
    public Admin insert(Admin admin) {
        this.adminDao.insert(admin);
        return admin;
    }

    /**
     * 修改数据
     *
     * @param admin 实例对象
     * @return 实例对象
     */
    @Override
    public int update(Admin admin) {
        return  adminDao.update(admin);
    }

    /**
     * 通过主键删除数据
     *
     * @param ids 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(List<Integer> ids) {

        return this.adminDao.delete("stu.admin", StringUtil.listToString(ids)) > 0;
    }

    @Override
    public Admin login(String account, String password) {
        return adminDao.login(account,password);
    }

    @Override
    public List<Admin> queryAll(AdminQuery admin) {
        return null;
    }
}