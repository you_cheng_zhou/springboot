package cn.edu.lingnan.demosecond.service;

import cn.edu.lingnan.demosecond.entity.Admin;
import cn.edu.lingnan.demosecond.entity.AdminQuery;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Admin)表服务接口
 *
 * @author makejava
 * @since 2020-05-25 17:15:05
 */
public interface AdminService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Admin queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    public IPage<Admin> queryAllByLimit(int offset, int limit, AdminQuery bean);
    /**
     * 新增数据
     *
     * @param admin 实例对象
     * @return 实例对象
     */
    Admin insert(Admin admin);

    /**
     * 修改数据
     *
     * @param admin 实例对象
     * @return 实例对象
     */
    int update(Admin admin);

    /**
     * 通过主键删除数据
     *
     * @param ids 主键
     * @return 是否成功
     */
    boolean deleteById(@Param("ids") List<Integer> ids);

    //登录
    Admin login(String account,String password);

    List<Admin> queryAll(AdminQuery admin);
}