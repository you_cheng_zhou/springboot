package cn.edu.lingnan.demosecond.service.impl;

import cn.edu.lingnan.demosecond.entity.MenuVo;
import cn.edu.lingnan.demosecond.entity.Student;
import cn.edu.lingnan.demosecond.entity.SystemMenu;
import cn.edu.lingnan.demosecond.dao.SystemMenuDao;
import cn.edu.lingnan.demosecond.service.SystemMenuService;
import cn.edu.lingnan.demosecond.util.TreeUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统菜单表(SystemMenu)表服务实现类
 *
 * @author makejava
 * @since 2020-05-27 16:51:42
 */
@Service("systemMenuService")
public class SystemMenuServiceImpl implements SystemMenuService {
    @Resource
    private SystemMenuDao systemMenuDao;

    @Override
    public  List<SystemMenu> queryAll() {

        return systemMenuDao.queryAll(null);
    }


    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public SystemMenu queryById(Integer id) {
        return this.systemMenuDao.queryById(id);
    }

    /**
     * 分页查询
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @param bean
     * @return
     */
    @Override
    public IPage<SystemMenu> queryAllByLimit(int offset, int limit,SystemMenu bean) {
        Page<SystemMenu> page=new Page<>(offset,limit);
        page.setRecords(systemMenuDao.queryAllByLimit(page,bean));
        return page;
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */


    /**
     * 新增数据
     *
     * @param systemMenu 实例对象
     * @return 实例对象
     */
    @Override
    public SystemMenu insert(SystemMenu systemMenu) {
        systemMenu.setStatus(1);
        systemMenu.setTarget("_self");
        this.systemMenuDao.insert(systemMenu);
        return systemMenu;
    }

    /**
     * 修改数据
     *
     * @param systemMenu 实例对象
     * @return 实例对象
     */
    @Override
    public Boolean update(SystemMenu systemMenu) {
        return systemMenuDao.update(systemMenu) >0;
    }

    /**
     * 通过主键删除数据
     *
     * @param ids 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(List<Integer> ids) {
        if (ids == null || ids.size() == 0) {
            return false;
        }

        StringBuffer sb = new StringBuffer("id in (");
        for (Integer item : ids) {
            sb.append("'");
            sb.append(item);
            sb.append("',");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        sb.append(")");

        return systemMenuDao.delete("stu.system_menu", sb.toString()) > 0;
    }
}