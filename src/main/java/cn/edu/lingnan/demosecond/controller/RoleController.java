package cn.edu.lingnan.demosecond.controller;

import cn.edu.lingnan.demosecond.entity.Role;
import cn.edu.lingnan.demosecond.entity.Role;
import cn.edu.lingnan.demosecond.entity.common.CommonResult;
import cn.edu.lingnan.demosecond.service.RoleService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * (Role)表控制层
 *
 * @author makejava
 * @since 2020-06-05 16:41:47
 */
@Controller
@RequestMapping("role")
public class RoleController {
    /**
     * 服务对象
     */
    @Resource
    private RoleService roleService;

    @PostMapping("save")
    public boolean save(Role bean){
        if (bean.getId()!=null){
            //修改
            return roleService.update(bean) > 0;
        }
        //添加
        return roleService.insert(bean) > 0;
    }

    @GetMapping("/")
    public String toAdd(Model model){
        model.addAttribute("bean",new Role());
        return "menu/role_add";
    }

    @GetMapping("/{id}")
    public String toEdit(@PathVariable("id") Integer id,Model model){
        Role role = roleService.queryById(id);
        model.addAttribute("bean",role);
        return "menu/role_add";
    }


    @DeleteMapping("/{ids}")
    @ResponseBody
    public boolean deleteById(@PathVariable("ids") Integer[] ids){
        if (ids.length==0 || ids==null){
            return false;
        }
        return roleService.deleteById(Arrays.asList(ids));
    }



    @PostMapping("queryAll")
    @ResponseBody
    public Object queryAll(Integer page, Integer limit, Role bean){
        CommonResult<Role> result=new CommonResult<>();
        IPage<Role> iPage = roleService.queryAllByLimit(page,limit,bean);
        result.setCode(0);
        result.setCount(iPage.getTotal());
        result.setData(iPage.getRecords());
        return result;
    }

    /**
     * 去到菜单列表
     * @return
     */
    @RequestMapping("toList")
    public String toList(){
        return "menu/role_list";
    }

    @RequestMapping("toAuthList/{id}")
    //@ResponseBody
    public String toAuthList(@PathVariable("id") Integer id,Model model){
        model.addAttribute("role", roleService.queryById(id));
        return "menu/role_auth_list";
    }

    @PostMapping("queryMenus")
    @ResponseBody
    public Object queryMenus(String menus){
       return roleService.getAuth(menus);
    }



}