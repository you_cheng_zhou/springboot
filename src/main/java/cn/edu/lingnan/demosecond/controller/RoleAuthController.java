/*
package cn.edu.lingnan.demosecond.controller;

import cn.edu.lingnan.demosecond.entity.Role;
import cn.edu.lingnan.demosecond.entity.RoleAuth;
import cn.edu.lingnan.demosecond.entity.common.CommonResult;
import cn.edu.lingnan.demosecond.service.RoleAuthService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;

*/
/**
 * (RoleAuth)表控制层
 *
 * @author makejava
 * @since 2020-06-05 16:41:47
 *//*

@Controller
@RequestMapping("roleAuth")
public class RoleAuthController {
    */
/**
     * 服务对象
     *//*

    @Resource
    private RoleAuthService roleAuthService;

    @PostMapping("save")
    public boolean save(RoleAuth bean){
        if (bean.getId()!=null){
            //修改
            return roleAuthService.update(bean) > 0;
        }
        //添加
        return roleAuthService.insert(bean) > 0;
    }

    @GetMapping("/")
    public String toAdd(Model model){
        model.addAttribute("bean",new Role());
        return "menu/role_add";
    }

    @GetMapping("/{id}")
    public String toEdit(@PathVariable("id") Integer id,Model model){
        RoleAuth roleAuth = roleAuthService.queryById(id);
        model.addAttribute("bean",roleAuth);
        return "menu/role_add";
    }


    @DeleteMapping("/{ids}")
    @ResponseBody
    public boolean deleteById(@PathVariable("ids") Integer[] ids){
        if (ids.length==0 || ids==null){
            return false;
        }
        return roleAuthService.deleteById(Arrays.asList(ids));
    }



    @PostMapping("queryAll")
    @ResponseBody
    public Object queryAll(Integer page, Integer limit, RoleAuth bean){
        CommonResult<Role> result=new CommonResult<>();
        IPage<RoleAuth> iPage = roleAuthService.queryAllByLimit(page,limit,bean);
        result.setCode(0);
        result.setCount(iPage.getTotal());
        result.setData(iPage.getRecords());
        return result;
    }

    */
/**
     * 去到菜单列表
     * @return
     *//*

    @RequestMapping("toList")
    public String toList(){
        return "menu/role_list";
    }

}*/
