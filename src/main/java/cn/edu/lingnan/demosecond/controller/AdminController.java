package cn.edu.lingnan.demosecond.controller;

import cn.edu.lingnan.demosecond.entity.Admin;
import cn.edu.lingnan.demosecond.entity.AdminQuery;
import cn.edu.lingnan.demosecond.entity.Student;
import cn.edu.lingnan.demosecond.entity.common.CommonResult;
import cn.edu.lingnan.demosecond.listener.StudentExcelListener;
import cn.edu.lingnan.demosecond.service.AdminService;
import cn.edu.lingnan.demosecond.service.StudentService;
import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * (Admin)表控制层
 *
 * @author makejava
 * @since 2020-05-25 17:15:06
 */
@Controller
@RequestMapping("admin")
public class AdminController {
    private Logger logger= LoggerFactory.getLogger(getClass());
    /**
     * 服务对象
     */
    @Resource
    @Autowired
    private AdminService adminService;
    @Autowired
    private StudentService studentService;

    @RequestMapping("tologin")
    //@GetMapping("admin/tologin")
    public String tologin(){
        logger.trace("低级别");
        logger.debug("老二");
        logger.info("老三");
        logger.warn("老五");
        logger.error("老师");

        return "login";
    }

    @RequestMapping("toWelcome")
    public String toWelcome(){
        return "welcome";
    }


    @RequestMapping("toList")
    //@GetMapping("admin/toList")
    public String toList(){
        return "user/admin_list";
    }

    //@RequestMapping("toAdd")
    @GetMapping("/")
    public String toAdd(Model model){
        model.addAttribute("bean",new Admin());
        return "user/admin_add";
    }

    //@RequestMapping("toEdit")
    @GetMapping("/{id}")
    public String toEdit(@PathVariable("id") Integer id,Model model){
        Admin admin = adminService.queryById(id);
        model.addAttribute("bean",admin);
        return "user/admin_add";
    }


    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    @ResponseBody
    public Admin selectOne(Integer id) {
        return this.adminService.queryById(id);
    }

    @PostMapping("insert")
    @ResponseBody
    public Admin insert(Admin admin){
        return adminService.insert(admin);
    }

    @PostMapping("login")
    public String login(String account, String password, Model model, HttpSession session){
        Admin admin = adminService.login(account, password);
        if (admin==null){
            model.addAttribute("msg","账号或密码错误");
            return "login";
        }
        //使用Cookie记住账号密码(已完成)
        //将用户信息存入到session（已完成）
        session.setAttribute("account",account);
        session.setAttribute("password",password);
        return "index";
    }

    @PostMapping("queryAll")
    @ResponseBody
    public Object queryAll(Integer page,Integer limit, AdminQuery admin){
        CommonResult<Admin> result=new CommonResult<>();
        IPage<Admin> iPage =adminService.queryAllByLimit(page,limit,admin);
        result.setCode(0);
        result.setCount(iPage.getTotal());
        result.setData(iPage.getRecords());
        return result;
    }

   /* @PostMapping("save")
    private String save(Admin admin){
        admin.setRegData(new Date());
        adminService.insert(admin);
        return "user/admin_list";
    }*/

    @PostMapping("save")
    @ResponseBody
    private Object save(Admin bean){
        boolean res =false;
        //判断账号是否存在
        //判断是添加还是编辑
        if (bean.getId() != null) {
            //编辑
            res = adminService.update(bean) > 0;
        } else {
            //添加
            bean.setRegData(new Date());
            res = adminService.insert(bean).getId() != null;
        }
        return res;
    }

    //@PostMapping("deleteById")
    @DeleteMapping("/{ids}")
    @ResponseBody
    public boolean deleteById(@PathVariable("ids") Integer[] ids){
        if (ids == null || ids.length==0){
            return false;
        }
        adminService.deleteById(Arrays.asList(ids));
        System.out.println(":::::::::::::::::::::::::::::");
        return true;

    }

    /**
     * 文件上传
     */

    @PostMapping("upload")
    @ResponseBody
    public Object upload(MultipartFile file) {
        CommonResult<String>result=new CommonResult<>();
        try{
            EasyExcel.read(file.getInputStream(), Student.class,new StudentExcelListener(studentService)).sheet().doRead();
        }catch (IOException e){
            e.printStackTrace();
            result.setMsg("Excel上传出错");
        }
        result.setData(file.getOriginalFilename());

        return "ok";
    }




}