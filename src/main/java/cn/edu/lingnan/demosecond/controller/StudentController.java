package cn.edu.lingnan.demosecond.controller;

import cn.edu.lingnan.demosecond.entity.Student;
import cn.edu.lingnan.demosecond.entity.common.CommonResult;
import cn.edu.lingnan.demosecond.service.StudentService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * (Student)表控制层
 *
 * @author makejava
 * @since 2020-05-31 20:41:08
 */
@Controller
@RequestMapping("student")
public class StudentController {
    /**
     * 服务对象
     */
    @Resource
    private StudentService studentService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Student selectOne(Integer id) {
        return this.studentService.queryById(id);
    }


    /**
     * 去到编辑页面
     * @param id
     * @param model
     * @return
     */

    //@RequestMapping("toStuEdit")
    @GetMapping("/{id}")
    public String toStuEdit(@PathVariable("id") Integer id,Model model){
        Student student=studentService.queryById(id);
        model.addAttribute("StuBean",student);
        return "user/student_add";
    }

    /**
     * 添加学生和修改学生
     * @param student
     * @return
     */
    @PostMapping("save")
    public boolean save(Student student){
        if (student.getId()!=null){
            //修改
            return studentService.update(student) >0;
        }
        //添加
        return studentService.insert(student).getId()!=null;
    }

    /**
     * 去到学生添加页面
     * @param model
     * @return
     */
   // @RequestMapping("toStuAdd")
    @GetMapping("/")
    public String toStuAdd(Model model){
        model.addAttribute("StuBean",new Student());
        return "user/student_add";
    }


    /**
     * 批量删除和单个删除
     * @param ids
     * @return
     */

    //@PostMapping("deleteById")
    @DeleteMapping("/{ids}")
    @ResponseBody
    public boolean deleteById(@PathVariable("ids") Integer[] ids){
        if (ids.length==0 || ids==null){
            return false;
        }
        studentService.deleteByIds(Arrays.asList(ids));
        //System.out.println(":::::::::::::::::::::::::::::");
        return true;
    }

    /**
     * 分页查询
     * @param page
     * @param limit
     * @param bean
     * @return
     */
    @PostMapping("queryAll")
    @ResponseBody
    public Object queryAll(Integer page,Integer limit,Student bean){
        CommonResult<Student> result=new CommonResult<>();
        IPage<Student> iPage = studentService.queryAllByLimit(page,limit,bean);
        result.setCode(0);
        result.setCount(iPage.getTotal());
        result.setData(iPage.getRecords());
        return result;
    }

    /**
     * 去到学生列表
     * @return
     */
    @RequestMapping("toStuList")
    public String toStuList(){
        return "user/student_list";
    }


}