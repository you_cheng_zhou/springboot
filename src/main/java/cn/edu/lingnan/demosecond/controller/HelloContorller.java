package cn.edu.lingnan.demosecond.controller;

import cn.edu.lingnan.demosecond.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloContorller {

    @Value("${nue.className}")
    private String className;

    @Value("${nue.count}")
    private String count;

    @Value("${nue.teacher.name}")
    private String name;

    @Value("${nue.teacher.age}")
    private String age;

    //是组件可自动注入
    @Autowired
    private Teacher teacher;


    @RequestMapping("/hello")
    @ResponseBody
    public String hello(){
        return className+"  "+ count+"  "+name+"  "+age+"  "+teacher+"hello springboot";
    }
}
