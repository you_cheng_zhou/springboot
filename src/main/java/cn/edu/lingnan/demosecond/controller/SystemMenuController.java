package cn.edu.lingnan.demosecond.controller;

import cn.edu.lingnan.demosecond.entity.MenuVo;
import cn.edu.lingnan.demosecond.entity.Student;
import cn.edu.lingnan.demosecond.entity.SystemMenu;
import cn.edu.lingnan.demosecond.entity.common.CommonResult;
import cn.edu.lingnan.demosecond.service.SystemMenuService;
import cn.edu.lingnan.demosecond.util.TreeUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * 系统菜单表(SystemMenu)表控制层
 *
 * @author makejava
 * @since 2020-05-27 16:51:42
 */
@Controller
@RequestMapping("systemMenu")
public class SystemMenuController {
    /**
     * 服务对象
     */
    @Resource
    private SystemMenuService systemMenuService;


    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public SystemMenu selectOne(Integer id) {
        return this.systemMenuService.queryById(id);
    }

    @GetMapping("menus")
    @ResponseBody
    public Map<String,Object> menus()
    {
        Map<String, Object> map = new HashMap<>(16);
        Map<String,Object> home = new HashMap<>(16);
        Map<String,Object> logo = new HashMap<>(16);
        List<SystemMenu> menuList = systemMenuService.queryAll();
        List<MenuVo> menuInfo = new ArrayList<>();
        for (SystemMenu e : menuList) {
            MenuVo menuVO = new MenuVo();
            menuVO.setId(e.getId());
            menuVO.setPid(e.getPid());
            menuVO.setHref(e.getHref());
            menuVO.setTitle(e.getTitle());
            menuVO.setIcon(e.getIcon());
            menuVO.setTarget(e.getTarget());
            menuInfo.add(menuVO);
        }
        map.put("menuInfo", TreeUtil.toTree(menuInfo, 0));
        home.put("title","首页");
        home.put("href","/page/welcome-1");//控制器路由,自行定义
        logo.put("title","后台管理系统");
        logo.put("image","/static/images/back.jpg");//静态资源文件路径,可使用默认的logo.png
        map.put("homeInfo", "{title: '首页',href: '/ruge-web-admin/page/welcome.html'}}");
        map.put("logoInfo", "{title: 'RUGE ADMIN',image: 'images/logo.png'}");
        return map;
    }

    @GetMapping("getAuths")
    @ResponseBody
    public Object getAuths(){
        CommonResult<SystemMenu> result = new CommonResult<>();
        List<SystemMenu> list = systemMenuService.queryAll();
        result.setCode(0);
        result.setCount((long) list.size());
        result.setData(list);
        return result;
   }


    @PostMapping("save")
    public boolean save(SystemMenu bean){
        if (bean.getId()!=null){
            //修改
            return systemMenuService.update(bean);
        }
        //添加
        return systemMenuService.insert(bean).getId()!=null;
    }

    @GetMapping("/")
    public String toAdd(Model model){
        model.addAttribute("bean",new SystemMenu());
        return "menu/menu_add";
    }

    @GetMapping("/{id}")
    public String toEdit(@PathVariable("id") Integer id,Model model){
        SystemMenu systemMenu = systemMenuService.queryById(id);
        model.addAttribute("bean",systemMenu);
        return "menu/menu_add";
    }


    @DeleteMapping("/{ids}")
    @ResponseBody
    public boolean deleteById(@PathVariable("ids") Integer[] ids){
        if (ids.length==0 || ids==null){
            return false;
        }
        return systemMenuService.deleteById(Arrays.asList(ids));
    }



    @PostMapping("queryAll")
    @ResponseBody
    public Object queryAll(Integer page, Integer limit, SystemMenu bean){
        CommonResult<SystemMenu> result=new CommonResult<>();
        IPage<SystemMenu> iPage = systemMenuService.queryAllByLimit(page,limit,bean);
        result.setCode(0);
        result.setCount(iPage.getTotal());
        result.setData(iPage.getRecords());
        return result;
    }

    /**
     * 去到菜单列表
     * @return
     */
    @RequestMapping("toList")
    public String toList(){
        return "menu/menu_list";
    }





}