package cn.edu.lingnan.demosecond.dao;

import cn.edu.lingnan.demosecond.entity.RoleAuth;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * (RoleAuth)表数据库访问层
 *
 * @author makejava
 * @since 2020-06-05 16:41:47
 */
@Mapper
@Repository
public interface RoleAuthDao extends BaseDao<RoleAuth> {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    RoleAuth queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<RoleAuth> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param roleAuth 实例对象
     * @return 对象列表
     */
    List<RoleAuth> queryAll(IPage<RoleAuth> roleAuth,RoleAuth bean);

    /**
     * 新增数据
     *
     * @param roleAuth 实例对象
     * @return 影响行数
     */
  /*  int insert(RoleAuth roleAuth);*/

    /**
     * 修改数据
     *
     * @param roleAuth 实例对象
     * @return 影响行数
     */
  /*  int update(RoleAuth roleAuth);
*/
    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
  /*  int deleteById(Integer id);*/

}