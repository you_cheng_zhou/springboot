package cn.edu.lingnan.demosecond.dao;

import cn.edu.lingnan.demosecond.entity.Student;
import cn.edu.lingnan.demosecond.entity.SystemMenu;
import cn.edu.lingnan.demosecond.util.MySqlPrivider;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 系统菜单表(SystemMenu)表数据库访问层
 *
 * @author makejava
 * @since 2020-05-27 16:51:42
 */
@Mapper
@Repository
public interface SystemMenuDao extends BaseDao<SystemMenu> {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    SystemMenu queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param查询起始位置
     * @param查询条数
     * @return 对象列表
     */
    List<SystemMenu> queryAllByLimit(IPage<SystemMenu> page, SystemMenu bean);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param systemMenu 实例对象
     * @return 对象列表
     */
    List<SystemMenu> queryAll(SystemMenu systemMenu);



    /**
     * 修改数据
     *
     * @param systemMenu 实例对象
     * @return 影响行数
     */
   /* int update(SystemMenu systemMenu);*/

    /**
     * 通过主键删除数据
     *
     * @param ids 主键
     * @return 影响行数
     */
   /* int deleteById(@Param("ids") List<Integer> ids);*/

}