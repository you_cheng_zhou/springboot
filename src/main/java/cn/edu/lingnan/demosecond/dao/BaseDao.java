package cn.edu.lingnan.demosecond.dao;

import cn.edu.lingnan.demosecond.entity.SystemMenu;
import cn.edu.lingnan.demosecond.util.MySqlPrivider;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

public interface BaseDao<T> {
    /**
     * 新增数据
     *
     * @param bean 实例对象
     * @return 影响行数
     */
    @InsertProvider(type = MySqlPrivider.class,method = MySqlPrivider.INSERT)
    int insert(T bean);

    @DeleteProvider(type = MySqlPrivider.class,method = MySqlPrivider.DELETE)
    int delete(@Param("table") String table, @Param("where") String where);

    @UpdateProvider(type = MySqlPrivider.class,method = MySqlPrivider.UPDATE)
    int update(T bean);


}
