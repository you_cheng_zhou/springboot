package cn.edu.lingnan.demosecond.listener;

import cn.edu.lingnan.demosecond.entity.Student;
import cn.edu.lingnan.demosecond.service.StudentService;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.ArrayList;
import java.util.List;

public class StudentExcelListener extends AnalysisEventListener<Student> {
    private List<Student> data=new ArrayList<>();
    private StudentService studentService;
    public StudentExcelListener(StudentService studentService){
        this.studentService=studentService;
    }

    @Override
    public void invoke(Student student, AnalysisContext analysisContext) {
        //解析数据保存到Student
        data.add(student);

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        //Excel 解析完毕后执行
        System.out.println(data);
        if(studentService!=null){
            studentService.insert(data);
        }


    }
}
